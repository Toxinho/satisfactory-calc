import { IState } from ".";
import { machinesById } from "../data/machines";

export function isBeltEnabled(state: IState) {
	return (beltId: number) => state.beltSettings[beltId];
}

export function isMachineEnabled(state: IState) {
	return (machineId: number) => state.machineSettings[machineId];
}

export function enabledMachines(state: IState) {
	return Object.keys(state.machineSettings)
	.filter((machineId) => state.machineSettings[machineId])
	.map((machineId) => machinesById[machineId]);
}
