import Vue from "vue";
import Vuex from "vuex";
import { IItemRate } from "../models";
import * as mutations from "./mutations";
import * as getters from "./getters";
import { belts } from "../data/belts";
import { itemsByName } from "../data/items";
import { machines } from "../data/machines";
import persistPlugin, { loadState } from "./persist";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export interface IState {
	persistState: boolean;
	requiredOutputs: IItemRate[];
	beltSettings: { [beltId: number]: boolean };
	machineSettings: { [machineId: number]: boolean };
}

export function defaultState(persist = true) {
	return {
		persistState: persist,
		requiredOutputs: [{ itemId: itemsByName["Reinforced Iron Plate"].id, rate: 60 }],
		// Which belts are enabled, default to all
		beltSettings: belts.reduce((settings, belt) => {
			settings[belt.id] = true;
			return settings;
		}, {}),
		machineSettings: machines.reduce((settings, machine) => {
			settings[machine.id] = true;
			return settings;
		}, {})
	} as IState;
}

const store = new Vuex.Store<IState>({
	state: loadState(),
	mutations,
	getters,
	plugins: [persistPlugin],
	strict: debug
});

export default store;

if (module.hot) {
	module.hot.accept(["./mutations", "./getters"], () => {
		// Require the updated modules
		const newMutations = require("./mutations").default;
		const newGetters = require("./getters").default;

		// Swap in the new modules and mutations
		store.hotUpdate({
			mutations: newMutations,
			getters: newGetters
		});
	});
}
