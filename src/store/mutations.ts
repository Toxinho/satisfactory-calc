import Vue from "vue";
import { IState, defaultState } from ".";
import { IItemRate } from "../models";

export function enablePersistence(state: IState, enabled: boolean) {
	state.persistState = enabled;
}

export function resetState(state: IState) {
	state = Object.assign(state, defaultState);
}

export function addRequiredOutput(state: IState, newOutput: IItemRate ) {
	state.requiredOutputs.push(newOutput);
}

export function removeRequiredOutput(state: IState, outputIndex: number) {
	state.requiredOutputs.splice(outputIndex, 1);
}

export function changeRequiredOutputRate(state: IState, payload: { outputIndex: number, newRate: number }) {
	if (payload.outputIndex < state.requiredOutputs.length) {
		state.requiredOutputs[payload.outputIndex].rate = payload.newRate;
	}
}

export function enableBelt(state: IState, payload: {beltId: number, enabled: boolean}) {
	Vue.set(state.beltSettings, payload.beltId, payload.enabled);
}

export function enableMachine(state: IState, payload: {machineId: number, enabled: boolean}) {
	Vue.set(state.machineSettings, payload.machineId, payload.enabled);
}
