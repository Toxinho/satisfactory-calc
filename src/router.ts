import Vue from "vue";
import VueRouter from "vue-router";

import Calculator from "./components/Calculator.vue";
const Graph = () => import("./components/Graph.vue");
import Settings from "./components/Settings.vue";

Vue.use(VueRouter);

export default new VueRouter({
	routes: [
		{ path: "/", component: Calculator },
		{ path: "/graph", component: Graph },
		{ path: "/settings", component: Settings }
	]
});
