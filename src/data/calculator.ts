import { IProductionStep, IItemRate, RecipeType, IMachine } from "../models";
import { recipes } from "./recipes";
import { itemsById } from "./items";

export function findBestRecipe(itemId: number) {
	// Determine which recipes will result in the output we want
	const options = recipes.filter((recipe) => recipe.output.itemId === itemId);
	if (options.length === 0) {
		throw new Error(`Could not find a recipe to make "${itemsById[itemId] ? itemsById[itemId].name : `item #${itemId}`}"`);
	}

	// Find the one with the highest baseRate
	// The harvest options have a baseRate of 0 which means they will always be picked last
	return options.reduce((best, option) => best.baseRate >= option.baseRate ? best : option, options[0]);
}

export function findBestMachine(type: RecipeType, enabledMachines: IMachine[]) {
	// Determine which machines will work for our recipe
	const options = enabledMachines.filter((machine) => machine.type === type);
	if (options.length === 0) {
		throw new Error(`Could not find a machine for type "${type}"`);
	}

	// Find the machine with the highest rateMultiplier
	return options.reduce((best, option) => best.rateMultiplier >= option.rateMultiplier ? best : option, options[0]);
}

export function calculateMachinesNeeded(requiredRate: number, recipeRate: number, machineRateMul: number, quanityPerRecipe: number) {
	return requiredRate / (recipeRate * machineRateMul * quanityPerRecipe);
}

export function calculateSteps(requiredItems: IItemRate[], settings: { enabledMachines: IMachine[] }, level: number = 0): IProductionStep[] {
	// Recursivly, calculate what items are needed to create the required items
	return requiredItems.map((requirement) => {
		// Find the best recipe for this requirement
		const recipe = findBestRecipe(requirement.itemId);

		// Find the best machine for this recipie type
		const machine = findBestMachine(recipe.type, settings.enabledMachines);

		// How many of this machine do we need to fufil the required rate?
		const quantity = calculateMachinesNeeded(requirement.rate, recipe.baseRate, machine.rateMultiplier, recipe.output.quantity);

		// What requirements does this step have?
		// The amount of input required:
		// (# of recipe input) * (reciper iterations / m) * (# of machines)
		const childRequirements = recipe.inputs.map((input) => ({
			itemId: input.itemId,
			rate: input.quantity * recipe.baseRate * quantity
		}));

		return {
			recipeId: recipe.id,
			machineId: machine.id,
			quantity,
			required: calculateSteps(childRequirements, settings, level + 1),
			level
		};
	});
}
