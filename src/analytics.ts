import Vue from "vue";
import VueAnalytics from "vue-analytics";
import router from "./router";

Vue.use(VueAnalytics, {
	id: "UA-64271266-3",
	router,
	debug: {
		sendHitTask: process.env.NODE_ENV === "production"
	}
});
